package refacto_maven;

import static org.junit.Assert.*;

import org.junit.Test;

public class RentalTest {

	@Test
	public void test() {
		
		// Assert
		double result;
		final double requiredResult = 15;
		Movie releaseMovie = new NewReleaseMovie("#title", 1);
		Rental rental = new Rental(releaseMovie, 5);
		
		// Act
		result = rental.amoutFor();
		
		//  Assert
		assertEquals(result, requiredResult, 0);
	}

}
