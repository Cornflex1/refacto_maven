package refacto_maven;

import static org.junit.Assert.*;

import org.junit.Test;

public class ChildrensMovieTest {

	@Test
	public void testAmoutFor() {
		
		// Arrange 
		Movie childrenMovie = new ChildrensMovie("#title", 2);
		double result;
		final double requiredResult = 4.5;
		
		// Act
		result = childrenMovie.amoutFor(5);
		
		// Assert
		assertEquals(result, requiredResult, 0);
		
	}

}
