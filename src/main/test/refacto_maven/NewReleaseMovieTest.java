package refacto_maven;

import static org.junit.Assert.*;

import org.junit.Test;

public class NewReleaseMovieTest {

	@Test
	public void testAmoutFor() {
		
		// Arrange
		Movie newReleaseMovie = new NewReleaseMovie("#title", 2);
		double result;
		final double requiredResult = 15;
		
		// Act
		result = newReleaseMovie.amoutFor(5);
		
		// Assert
		assertEquals(result, requiredResult, 0);
		
	}
}
