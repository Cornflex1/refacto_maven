package refacto_maven;

import static org.junit.Assert.*;

import org.junit.Test;

public class RegularMovieTest {

	@Test
	public void testAmoutFor() {
		
		// Arrange 
		Movie regularMovie = new RegularMovie("#title", 2);
		double result;
		final double requiredResult = 6.5;
		
		// Act
		result = regularMovie.amoutFor(5);
		
		// Assert
		assertEquals(result, requiredResult, 0);
		
	}

}
