package refacto_maven;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CustomerTest {

	@Test
	public void testAmoutFor() {
		
		// Arrange
		Statement statement = new Statement();
		Customer customer = new Customer("Jean Pierre", statement);
		Movie childrenMovie = new ChildrensMovie("Pirate des cara�bes", 2);
		Rental rental = new Rental(childrenMovie, 5);
		
		// Act
		customer.addRentals(rental);
		
		// Assert
		assertEquals(customer.getRentals().size(), 1);
	}
}