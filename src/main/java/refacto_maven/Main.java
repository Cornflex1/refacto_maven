package refacto_maven;

public class Main {

	public static void main(String[] a) {

		Movie m1 = new ChildrensMovie("M1", 0);
		Movie m2 = new NewReleaseMovie("M2", 1);
		Movie m3 = new NewReleaseMovie("M3", 1);
		Movie m4 = new RegularMovie("M4", 2);
		Movie m5 = new ChildrensMovie("M5", 2);
		
		Statement statement = new Statement();
		Customer c1 = new Customer("Moi", statement);
		
		Rental r1 = new Rental(m5, 5);

		c1.addRentals(r1);
		c1.addRentals(new Rental(m1, 10));
		c1.addRentals(new Rental(m3, 5));

		System.out.println(c1.statement());
	}
}
