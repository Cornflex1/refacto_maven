package refacto_maven;

import java.util.*;

public class Customer {

	private String name;

	private ArrayList<Rental> rentals;
	
	private IStatement statement;

	public Customer(String name, IStatement statement) {

		this.name = name;
		
		this.statement = statement;

		this.rentals = new ArrayList<Rental>();
	}

	public void addRentals(Rental arg) {

		rentals.add(arg);
	}

	public String getName() {

		return name;
	}

	public ArrayList<Rental> getRentals() {
		return rentals;
	}

	public String statement() {
		return this.statement.statement(rentals, this.getName());
	}
}

