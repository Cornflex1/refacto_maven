package refacto_maven;

public abstract class Movie {
	private String title;
	private int priceCode;

	public Movie(String title, int priceCode) {
		this.title = title;
		this.priceCode = priceCode;
	}

	public String getTitle() {
		return title;
	}

	public int getPriceCode() {
		return priceCode;
	}

	public void setPriceCode(int newCode) {
		priceCode = newCode;
	}
	
	public abstract double amoutFor(int daysRented);
}
