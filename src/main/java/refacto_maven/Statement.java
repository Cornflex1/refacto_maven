package refacto_maven;

import java.util.ArrayList;

public class Statement implements IStatement {

	public String statement(ArrayList<Rental> rentals , String name) {

		double totalAmount = 0;

		int frequentRenterPoints = 0;

		String result = "Rental Record for " + name + "\n";

		for (Rental each : rentals) {

			double thisAmount = each.amoutFor();

			// add frequent renter points
			frequentRenterPoints++;

			// add bonus for a two day new release rental
			if (each.getMovie() instanceof NewReleaseMovie) {

				frequentRenterPoints++;
			}

			// show figures for this rental
			result += "\t" + each.getMovie().getTitle()

					+ "\t" + thisAmount + "\n";

			totalAmount += thisAmount;
		}

		// add footer lines
		result += "Amount owed is "

				+ totalAmount

				+ "\n";

		result += "You earned "

				+ frequentRenterPoints

				+ "frequent renter points ";

		return result;
	}
}
