package refacto_maven;

public class ChildrensMovie extends Movie {

	public ChildrensMovie(String title, int priceCode) {
		super(title, priceCode);
	}
	
	@Override
	public double amoutFor(int daysRented) {
		
		double thisAmount = 1.5;

		if (daysRented > 3) {

			thisAmount += (daysRented - 3) * 1.5;
		}
		return thisAmount;
	}

}
