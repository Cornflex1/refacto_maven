package refacto_maven;

public class Rental {

	private int daysRented;

	private Movie movie;

	public Rental(Movie movie, int daysRented) {

		this.movie = movie;

		this.daysRented = daysRented;
	}

	public int getDaysRented() {

		return daysRented;
	}

	public Movie getMovie() {

		return movie;
	}

	double amoutFor() {

		return movie.amoutFor(daysRented);
	}
}

