package refacto_maven;

public class NewReleaseMovie extends Movie {

	public NewReleaseMovie(String title, int priceCode) {
		super(title, priceCode);
	}
	
	@Override
	public double amoutFor(int daysRented) {
		double total = (double) daysRented * 3; // to ignore
		return total ;
	}

}
